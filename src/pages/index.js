import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"
import ReactChartkick, { ScatterChart, AreaChart, LineChart } from 'react-chartkick'
import Chart from 'chart.js'
import {PolarAreaChart} from 'react-chartjs'
import chart3 from "../images/image.png";
import chart4 from "../images/image2.png";

ReactChartkick.addAdapter(Chart)

var data1 = {
  dataset :[["2000/2001",35.361111111111114], ["2001/2002",37.111111111111114], ["2002/2003",38.916666666666664 ], ["2003/2004",39.05555555555556], ["2004/2005",39.05555555555556],["2005/2006",40.44444444444444],["2006/2007",41.75],["2007/2008",50.22222222222222],["2008/2009",50.666666666666664], ["2009/2010",50.611111111111114]],
  type: 'polarArea',
  options: {

  }

}

var data2 = {
    dataset :[[4289432,4681], [4346218,4756], [4409732,4806], [4483588,4899], [4538360,5015]],
    type: 'polarArea',
    options: {
  
    }
  
  }
  
  

  
  
  
  
  




const IndexPage = () => (
  <Layout>
    <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />

    <h2> Average age of Doctors over time</h2> 
    <AreaChart data={data1.dataset} xtitle="Year" ytitle="Age" />
    
    <h2> Doctors General Practice by BC Popolation over 5 Years period </h2> 

    <LineChart data={data2.dataset} xtitle="Population" ytitle="Doctors"  title="y = 0.0013x - 788.63
R² = 0.9753"/>

<h1> No of Doctors per specialization in 2001</h1>
<img src={chart3} />
   
<h1> No of Doctors per specialization in 2010</h1>
<img src={chart4} />

  </Layout>
)

export default IndexPage
